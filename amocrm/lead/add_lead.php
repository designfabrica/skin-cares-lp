<?php


use AmoCRM\Contact;
use AmoCRM\Handler;
use AmoCRM\Lead;

require __DIR__ . "/../libs/amoapi.php";

$name = $_POST['name'];
$original_phone = $_POST['phone'];
$source = $_POST['source'];

sendMail($name, $original_phone, $source);

$api = new Handler('bsolutions', 'kairkanuly@gmail.com');

$lead = new Lead();

$tags = array();
array_push($tags, 'заявка с сайта');

$sources = explode(";", $source);

foreach ($sources as $s) {
    array_push($tags, $s);
}


$lead
    ->setName($name . " - koreanpony.kz")
    ->setResponsibleUserId(1335936)// Aibol Kussain
    ->setStatusId(14041287) // Первичный контакт
    ->setTags($tags);

commit($lead, $api);
$lead_id = $api->last_insert_id;

$phone = formatPhone($original_phone);

$contact = searchContact($api, $phone);

if (!$contact) {
    $contact = new Contact();
    $contact_phone = "8" . $phone;
    $contact
        ->setName($name)
        ->setResponsibleUserId(1335936)
        ->setCustomField($api->config["ContactPhone"], $contact_phone, 456446)
        ->setLinkedLeadsId($lead_id);
    commit($contact, $api);
} else {
    $updated_contact = new Contact();
    $updated_contact
        ->setName($contact->name)
        ->setUpdate($contact->id, $contact->last_modified + 1)
        ->setResponsibleUserId($contact->responsible_user_id)
        ->setLinkedLeadsId($contact->linked_leads_id);
    $updated_contact->setLinkedLeadsId($lead_id);
    commit($updated_contact, $api);
}

function sendMail($name, $phone, $source)
{
    $to = "aibolikmarketing@gmail.com";
    $subject = "New application from Skin Cares"; // Choose a custom subject (not mandatory)

    $body = "У Нас новая заявка.\n\nИмя: " . $name . "\n" . "Телефон: " . $phone . "\n" . "Источник: " . $source;

    $body = wordwrap($body, 70, "\r\n");
    echo $body;

    $from = "no-reply@koreanpony.kz";
    $headers = "From:" . $from . "\r\n";
//$headers .= 'Bcc: 41b0l123@gmail.com' . "\r\n";
    $headers .= "X-Mailer: PHP/" . phpversion();


    if ($name != '' && $phone != '') {
        echo "\n" . $to;
        echo "\n" . $from;
        echo "\n" . $headers;

        if (mail($to, $subject, $body, $headers)) {
            echo '<p style="color:#66A325;">Thanks! Your message has been sent.</p>';
        } else {
            echo '<p style="color:#F84B3C;">Something went wrong, go back and try again!</p>';
        }

    } else {
        echo '<p style="color:#F84B3C;">You need to fill in all required fields!</p>';

    }
}

?>