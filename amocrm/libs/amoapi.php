<?php


use AmoCRM\Request;

require __DIR__ ."/../vendor/autoload.php";

class Manager {
    public $name;
    public $phone_number;

    /**
     * Manager constructor.
     * @param $name
     * @param $phone_number
     */
    public function __construct($name, $phone_number)
    {
        $this->name = $name;
        $this->phone_number = $phone_number;
    }


}

function getResponsibleManager($responsible_manager_id, $api) {
    // Достаем менеджера лида
    $request_get = new Request(Request::GET, [], ['accounts', 'current']);
    $result = $api->request($request_get)->result;

    $users = $result->account->users;

    foreach ($users as $user) {
        if ($user->id == $responsible_manager_id) {
            return $user;
            break;
        }
    }

    return new Manager("Боков Павел", "+77081273797");
}

function getLead($lead_id, $api) {
    $request_get = new Request(Request::GET, ['id' => $lead_id], ['leads', 'list']);

    $result = $api->request($request_get)->result;

    return $result->leads[0];
}

function getContact($contact_id, $api) {
    $request_get = new Request(Request::GET, ['id' => $contact_id], ['contacts', 'list']);

    $result = $api->request($request_get)->result;

    return $result->contacts[0];
}

function commit($object, $api) {
    $api->request(new Request(Request::SET, $object));
}

function searchContact($api, $phone) {
    $request_get = new Request(Request::GET, ['query' => $phone], ['contacts', 'list']);

    $result = $api->request($request_get)->result;

    if(isset($result->contacts)) {
        return $result->contacts[0];
    }

    return false;
}

function formatPhone($phone) {
    $phone = preg_replace("/[^0-9]/", "", $phone);
    $phone = substr($phone, -10);
    return $phone;
}

?>