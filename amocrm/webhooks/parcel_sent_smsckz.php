<?php

use \AmoCRM\Handler;
use \AmoCRM\Note;

require __DIR__ . "/../libs/amoapi.php";
require __DIR__ . "/../libs/smsc_api.php";

$api = new Handler('bsolutions', 'kairkanuly@gmail.com');

$lead_id = "";

if (isset($_POST)) {
   $lead_id = $_POST["leads"]["status"][0]["id"];
} else {
   exit("Invalid leadId");
}

$lead = getLead($lead_id, $api);

$lead_custom_fields = $lead->custom_fields;
$contact_id = $lead->main_contact_id;

$contact = getContact($lead->main_contact_id, $api);

$contact_phone = "";
$contact_custom_fields = $contact->custom_fields;

$nakladnaya = "";

for ($i = 0; $i < count($lead_custom_fields); $i++) {
    switch ($lead_custom_fields[$i]->id) {
        case $api->config["NomerNakladnoy"]:
            $nakladnaya = $lead_custom_fields[$i]->values[0]->value;
            break;
    }
}

for ($i = 0; $i < count($contact_custom_fields); $i++) {
    if ($contact_custom_fields[$i]->id == $api->config["ContactPhone"]) {
        $contact_phone = $contact_custom_fields[$i]->values[0]->value;
        break;
    }
}

//Ваша посылка отправлена. Номер отслеживания заказа 19200002121 по ссылке http://bit.ly/k-pony. Спасибо, Ваш "Korean Pony"
// Vasha posyilka otpravlena. Nomer otslezhivaniya zakaza 19200002121 po ssyilke http://bit.ly/k-pony. Spasibo, Vash "Korean Pony"

//sendMail("POST - " . json_encode($_POST) . " END POST<br /><br /><br />" . "LEADID - " . $lead_id . " END LEADID<br /><br /><br />" . "LEAD - " . json_encode($lead) . " END LEAD<br /><br /><br />" . "CONTACT - " . json_encode($contact) . " END CONTACT<br /><br /><br />" . "NAKLADNAYA - " . $nakladnaya . "<br /><br /><br />" . "PHONE - " . $contact_phone . "<br /><br /><br />");

if(!isset($nakladnaya) || strlen($nakladnaya) < 11) {
    exit("Invalid nakladnaya number");
}

$sms_content = "Vasha posyilka otpravlena. ";
$sms_content .= "Nomer otslezhivaniya zakaza " . $nakladnaya;
$sms_content .= " po ssyilke http://bit.ly/k-pony. ";
$sms_content .= "\nSpasibo, Vash \"Korean Pony\"";

//sendMail($sms_content);

$contact_phone = preg_replace("/[^0-9]/", "", $contact_phone);
$contact_phone = '7' . substr($contact_phone, -10);

//print_r("\n\n\n" . $contact_phone);


list($sms_id, $sms_cnt, $cost, $balance) = send_sms($contact_phone, $sms_content, 1);
//$smsApi = new TargetSMS();

print_r("SMS ID - " . $sms_id . "\nSMS COUNT - " . $sms_cnt . "\nSMS COST - " . $cost . "\nBALANCE - " . $balance);

if($sms_id) {
    $note_text = "Отправлено СМС на номер - " . $contact_phone . ":\nID - " . $sms_id . "\nЦена за СМС - " . $cost . "\nБаланс на счете - " . $balance . "\nТекст сообщения:\n\n" . $sms_content;
    $note = new Note();
    $note->setElementId($lead_id);
    $note->setElementType(Note::TYPE_LEAD);
    $note->setNoteType(Note::COMMON);
    $note->setText($note_text);
    commit($note, $api);
    print_r($api->result);
}

//$response = $smsApi->sendSMS($sms_content, $contact_phone);

$from = "no-reply@koreanpony.kz";
$headers = "From:" . $from . "\r\n";
//$headers .= 'Bcc: 41b0l123@gmail.com' . "\r\n";
$headers .= "X-Mailer: PHP/" . phpversion();

if (mail ("aibolikmarketing@gmail.com", "Korean Pony Debugging", $sms_content, $headers)) {
    echo '<p style="color:#66A325;">Thanks! Your message has been sent.</p>';
} else {
    echo '<p style="color:#F84B3C;">Something went wrong, go back and try again!</p>';
}

//print_r($response);

//sendMail($sms_content . "\n\n" . $contact_phone);

function sendMail($content) {
    $from = "no-reply@koreanpony.kz";
    $headers = "From:" . $from . "\r\n";
//$headers .= 'Bcc: 41b0l123@gmail.com' . "\r\n";
    $headers .= "X-Mailer: PHP/" . phpversion();

    if (mail ("aibolikmarketing@gmail.com", "Korean Pony Debugging", $content, $headers)) {
        echo '<p style="color:#66A325;">Thanks! Your message has been sent.</p>';
    } else {
        echo '<p style="color:#F84B3C;">Something went wrong, go back and try again!</p>';
    }

}

/* Результат запроса сохраняется в свойстве "result" объекта \AmoCRM\Handler()
Содержит в себе объект, полученный от AmoCRM, какой конкретно - сверяйтесь с документацией для каждого метода
Ошибка запроса выбросит исключение */
//$api->result == false, если ответ пустой (то есть контакты с таким телефоном не найдены) */

?>
