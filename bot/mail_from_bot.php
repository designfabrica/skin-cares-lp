<!-- Contact form PHP script -->

<?php

use AmoCRM\Contact;
use AmoCRM\Handler;
use AmoCRM\Lead;
use AmoCRM\Note;

require __DIR__ . "/../amocrm/libs/amoapi.php";

$botName = $_POST["bot_name"];
$to = $_POST["to"];

$order_time = $_POST["order_time"];
$order_number = $_POST["order_number"];
$order_names = json_decode($_POST["order_names"], true);
$order_quantities = json_decode($_POST["order_quantities"], true);
$order_prices = json_decode($_POST["order_prices"], true);
$order_total = $_POST["order_total"];

$customer_name = $_POST["customer_name"];
$customer_phone = $_POST["customer_phone"];
$customer_address = $_POST["customer_address"];
$customer_info = $_POST["customer_info"];
$redirect_address = "http://46.101.149.220/backend/web/orders/view?id=" . $order_number;

$date = date("F, j, H:i", $order_time);

$ru_months = array( 'Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь' );
$en_months = array( 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December' );
$dateFormatted = str_replace( $en_months, $ru_months, $date );

$seperator = <<<SEP
<tr>
                    <td>
                        <table width="450" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
                            <tbody>
                                <tr>
                                    <td align="center" height="5" style="font-size:1px; line-height:1px;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td width="550" align="center" height="1" bgcolor="#d1d1d1" style="font-size:1px; line-height:1px;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="center" height="5" style="font-size:1px; line-height:1px;">&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
SEP;

$seperatorBold = <<<SEP2
<tr>
                    <td>
                        <table width="450" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
                            <tbody>
                                <tr>
                                    <td align="center" height="5" style="font-size:1px; line-height:1px;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td width="550" align="center" height="1" bgcolor="#000" style="font-size:1px; line-height:2px;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="center" height="5" style="font-size:1px; line-height:1px;">&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
SEP2;


$product = <<<PRODUCT
<tr>
                    <td>
                        <table width="450" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                            <tbody>
                                <tr>
                                    <td width="100%%">
                                        <table bgcolor="#ffffff" width="450" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <!-- start of left column -->
                                                        <table width="225" align="left" border="0" cellpadding="0" cellspacing="0" class="devicewidth">
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <!-- start of text content table -->
                                                                        <table width="225" align="left" border="0" cellpadding="0" cellspacing="0" class="devicewidth">
                                                                            <tbody>
                                                                                <!-- Content -->
                                                                                <tr>
                                                                                    <td>
                                                                                        <table width="225" align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidthinner">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td style="font-family: Helvetica, arial, sans-serif; font-size: 18px; color: #333333; line-height:24px;text-align:left;"
                                                                                                        st-title="2coltitle1">
                                                                                                        %s X %d
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <!--<tr>
                                                                     <td width="100%%" height="20"></td>
                                                                  </tr>-->
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <!-- end of Content -->
                                                                                <!-- end of content -->
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <!-- end of text content table -->
                                                            </tbody>
                                                        </table>
                                                        <!-- end of left column -->
                                                        <!-- start of right column -->
                                                        <table width="225" align="right" border="0" cellpadding="0" cellspacing="0" class="devicewidth">
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <!-- start of text content table -->
                                                                        <table width="225" align="left" border="0" cellpadding="0" cellspacing="0" class="devicewidth">
                                                                            <tbody>
                                                                                <!-- Content -->
                                                                                <tr>
                                                                                    <td>
                                                                                        <table width="225" align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidthinner">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td style="font-family: Helvetica, arial, sans-serif; font-size: 18px; color: #333333;line-height:24px; text-align:right;"
                                                                                                        st-title="2coltitle2">
                                                                                                        %d тг.
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <!-- end of Content -->
                                                                                <!-- end of content -->
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <!-- end of text content table -->
                                                            </tbody>
                                                        </table>
                                                        <!-- end of right column -->
                                                    </td>
                                                </tr>
                                                <!-- Spacing -->
                                                <!--<tr>
                                 <td width="100%%" height="10"></td>
                              </tr>-->
                                                <!-- Spacing -->
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
PRODUCT;

$total = <<<TOTAL
<tr>
                    <td>
                        <table width="450" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                            <tbody>
                                <tr>
                                    <td width="100%%">
                                        <table bgcolor="#ffffff" width="450" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <!-- start of left column -->
                                                        <table width="225" align="left" border="0" cellpadding="0" cellspacing="0" class="devicewidth">
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <!-- start of text content table -->
                                                                        <table width="225" align="left" border="0" cellpadding="0" cellspacing="0" class="devicewidth">
                                                                            <tbody>
                                                                                <!-- Content -->
                                                                                <tr>
                                                                                    <td>
                                                                                        <table width="225" align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidthinner">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td style="font-family: Helvetica, arial, sans-serif; font-size: 18px; font-weight: 600; color: #333333; line-height:24px;text-align:left;"
                                                                                                        st-title="2coltitle1">
                                                                                                        Итого
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <!--<tr>
                                                                     <td width="100%%" height="20"></td>
                                                                  </tr>-->
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <!-- end of Content -->
                                                                                <!-- end of content -->
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <!-- end of text content table -->
                                                            </tbody>
                                                        </table>
                                                        <!-- end of left column -->
                                                        <!-- start of right column -->
                                                        <table width="225" align="right" border="0" cellpadding="0" cellspacing="0" class="devicewidth">
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <!-- start of text content table -->
                                                                        <table width="225" align="left" border="0" cellpadding="0" cellspacing="0" class="devicewidth">
                                                                            <tbody>
                                                                                <!-- Content -->
                                                                                <tr>
                                                                                    <td>
                                                                                        <table width="225" align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidthinner">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td style="font-family: Helvetica, arial, sans-serif; font-size: 18px; font-weight: 600; color: #333333;line-height:24px; text-align:right;"
                                                                                                        st-title="2coltitle2">
                                                                                                        %d тг. 
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <!-- end of Content -->
                                                                                <!-- end of content -->
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <!-- end of text content table -->
                                                            </tbody>
                                                        </table>
                                                        <!-- end of right column -->
                                                    </td>
                                                </tr>
                                                <!-- Spacing -->
                                                <!--<tr>
                                 <td width="100%%" height="10"></td>
                              </tr>-->
                                                <!-- Spacing -->
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
TOTAL;

$details = <<<DETAILS
<table width="100%%" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="full-text">
            <tbody>
                <tr>
                    <td>
                        <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                            <tbody>
                                <tr>
                                    <td width="100%%">
                                        <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                                            <tbody>
                                                <!-- Spacing -->
                                                <tr>
                                                    <td height="20" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                                                </tr>
                                                <!-- Spacing -->
                                                <tr>
                                                    <td>
                                                        <table width="560" align="center" cellpadding="0" cellspacing="0" border="0" class="devicewidthinner">
                                                            <tbody>
                                                                <!-- Title -->
                                                                <tr>
                                                                    <td style="font-family: Helvetica, arial, sans-serif; font-size: 30px; color: #333333; text-align:center; line-height: 30px;"
                                                                        st-title="fulltext-title">
                                                                        <br> Детали заказа
                                                                    </td>
                                                                </tr>
                                                                <!-- End of Title -->
                                                                <!-- End of content -->
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <!-- Spacing -->
                                                <tr>
                                                    <td height="50" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                                                </tr>
                                                <!-- Spacing -->
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="450" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                            <tbody>
                                <tr>
                                    <td width="100%%">
                                        <table bgcolor="#ffffff" width="450" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <!-- start of left column -->
                                                        <table width="300" align="left" border="0" cellpadding="0" cellspacing="0" class="devicewidth">
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <!-- start of text content table -->
                                                                        <table width="300" align="left" border="0" cellpadding="0" cellspacing="0" class="devicewidth">
                                                                            <tbody>
                                                                                <!-- Content -->
                                                                                <tr>
                                                                                    <td>
                                                                                        <table width="300" align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidthinner">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td style="font-family: Helvetica, arial, sans-serif; font-size: 16px; color: #333333; line-height:24px;text-align:left;"
                                                                                                        st-title="2coltitle1">
                                                                                                        Имя клиента
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <!--<tr>
                                                                     <td width="100%%" height="20"></td>
                                                                  </tr>-->
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <!-- end of Content -->
                                                                                <!-- end of content -->
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <!-- end of text content table -->
                                                            </tbody>
                                                        </table>
                                                        <!-- end of left column -->
                                                        <!-- start of right column -->
                                                        <table width="150" align="right" border="0" cellpadding="0" cellspacing="0" class="devicewidth">
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <!-- start of text content table -->
                                                                        <table width="150" align="left" border="0" cellpadding="0" cellspacing="0" class="devicewidth">
                                                                            <tbody>
                                                                                <!-- Content -->
                                                                                <tr>
                                                                                    <td>
                                                                                        <table width="150" align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidthinner">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td style="font-family: Helvetica, arial, sans-serif; font-size: 16px; color: #333333;line-height:24px; text-align:right;"
                                                                                                        st-title="2coltitle2">
                                                                                                        {$customer_name}
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <!-- end of Content -->
                                                                                <!-- end of content -->
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <!-- end of text content table -->
                                                            </tbody>
                                                        </table>
                                                        <!-- end of right column -->
                                                    </td>
                                                </tr>
                                                <!-- Spacing -->
                                                <!--<tr>
                                 <td width="100%%" height="10"></td>
                              </tr>-->
                                                <!-- Spacing -->
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="450" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
                            <tbody>
                                <tr>
                                    <td align="center" height="10" style="font-size:1px; line-height:1px;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td width="550" align="center" height="1" bgcolor="#d1d1d1" style="font-size:1px; line-height:1px;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="center" height="10" style="font-size:1px; line-height:1px;">&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="450" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                            <tbody>
                                <tr>
                                    <td width="100%%">
                                        <table bgcolor="#ffffff" width="450" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <!-- start of left column -->
                                                        <table width="300" align="left" border="0" cellpadding="0" cellspacing="0" class="devicewidth">
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <!-- start of text content table -->
                                                                        <table width="300" align="left" border="0" cellpadding="0" cellspacing="0" class="devicewidth">
                                                                            <tbody>
                                                                                <!-- Content -->
                                                                                <tr>
                                                                                    <td>
                                                                                        <table width="300" align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidthinner">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td style="font-family: Helvetica, arial, sans-serif; font-size: 16px; color: #333333; line-height:24px;text-align:left;"
                                                                                                        st-title="2coltitle1">
                                                                                                        Телефон
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <!--<tr>
                                                                     <td width="100%%" height="20"></td>
                                                                  </tr>-->
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <!-- end of Content -->
                                                                                <!-- end of content -->
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <!-- end of text content table -->
                                                            </tbody>
                                                        </table>
                                                        <!-- end of left column -->
                                                        <!-- start of right column -->
                                                        <table width="150" align="right" border="0" cellpadding="0" cellspacing="0" class="devicewidth">
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <!-- start of text content table -->
                                                                        <table width="150" align="left" border="0" cellpadding="0" cellspacing="0" class="devicewidth">
                                                                            <tbody>
                                                                                <!-- Content -->
                                                                                <tr>
                                                                                    <td>
                                                                                        <table width="150" align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidthinner">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td style="font-family: Helvetica, arial, sans-serif; font-size: 16px; color: #333333;line-height:24px; text-align:right;"
                                                                                                        st-title="2coltitle2">
                                                                                                        {$customer_phone}
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <!-- end of Content -->
                                                                                <!-- end of content -->
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <!-- end of text content table -->
                                                            </tbody>
                                                        </table>
                                                        <!-- end of right column -->
                                                    </td>
                                                </tr>
                                                <!-- Spacing -->
                                                <!--<tr>
                                 <td width="100%%" height="10"></td>
                              </tr>-->
                                                <!-- Spacing -->
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="450" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
                            <tbody>
                                <tr>
                                    <td align="center" height="10" style="font-size:1px; line-height:1px;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td width="550" align="center" height="1" bgcolor="#d1d1d1" style="font-size:1px; line-height:1px;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="center" height="10" style="font-size:1px; line-height:1px;">&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="450" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                            <tbody>
                                <tr>
                                    <td width="100%%">
                                        <table bgcolor="#ffffff" width="450" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <!-- start of left column -->
                                                        <table width="300" align="left" border="0" cellpadding="0" cellspacing="0" class="devicewidth">
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <!-- start of text content table -->
                                                                        <table width="300" align="left" border="0" cellpadding="0" cellspacing="0" class="devicewidth">
                                                                            <tbody>
                                                                                <!-- Content -->
                                                                                <tr>
                                                                                    <td>
                                                                                        <table width="300" align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidthinner">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td style="font-family: Helvetica, arial, sans-serif; font-size: 16px; color: #333333; line-height:24px;text-align:left;"
                                                                                                        st-title="2coltitle1">
                                                                                                        Адрес
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <!--<tr>
                                                                     <td width="100%%" height="20"></td>
                                                                  </tr>-->
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <!-- end of Content -->
                                                                                <!-- end of content -->
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <!-- end of text content table -->
                                                            </tbody>
                                                        </table>
                                                        <!-- end of left column -->
                                                        <!-- start of right column -->
                                                        <table width="150" align="right" border="0" cellpadding="0" cellspacing="0" class="devicewidth">
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <!-- start of text content table -->
                                                                        <table width="150" align="left" border="0" cellpadding="0" cellspacing="0" class="devicewidth">
                                                                            <tbody>
                                                                                <!-- Content -->
                                                                                <tr>
                                                                                    <td>
                                                                                        <table width="150" align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidthinner">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td style="font-family: Helvetica, arial, sans-serif; font-size: 16px; color: #333333;line-height:24px; text-align:right;"
                                                                                                        st-title="2coltitle2">
                                                                                                        {$customer_address}
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <!-- end of Content -->
                                                                                <!-- end of content -->
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <!-- end of text content table -->
                                                            </tbody>
                                                        </table>
                                                        <!-- end of right column -->
                                                    </td>
                                                </tr>
                                                <!-- Spacing -->
                                                <!--<tr>
                                 <td width="100%%" height="10"></td>
                              </tr>-->
                                                <!-- Spacing -->
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="450" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
                            <tbody>
                                <tr>
                                    <td align="center" height="10" style="font-size:1px; line-height:1px;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td width="550" align="center" height="1" bgcolor="#d1d1d1" style="font-size:1px; line-height:1px;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="center" height="10" style="font-size:1px; line-height:1px;">&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="450" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                            <tbody>
                                <tr>
                                    <td width="100%%">
                                        <table bgcolor="#ffffff" width="450" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <!-- start of left column -->
                                                        <table width="300" align="left" border="0" cellpadding="0" cellspacing="0" class="devicewidth">
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <!-- start of text content table -->
                                                                        <table width="300" align="left" border="0" cellpadding="0" cellspacing="0" class="devicewidth">
                                                                            <tbody>
                                                                                <!-- Content -->
                                                                                <tr>
                                                                                    <td>
                                                                                        <table width="300" align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidthinner">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td style="font-family: Helvetica, arial, sans-serif; font-size: 16px; color: #333333; line-height:24px;text-align:left;"
                                                                                                        st-title="2coltitle1">
                                                                                                        Доп. информация
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <!--<tr>
                                                                     <td width="100%%" height="20"></td>
                                                                  </tr>-->
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <!-- end of Content -->
                                                                                <!-- end of content -->
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <!-- end of text content table -->
                                                            </tbody>
                                                        </table>
                                                        <!-- end of left column -->
                                                        <!-- start of right column -->
                                                        <table width="150" align="right" border="0" cellpadding="0" cellspacing="0" class="devicewidth">
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <!-- start of text content table -->
                                                                        <table width="150" align="left" border="0" cellpadding="0" cellspacing="0" class="devicewidth">
                                                                            <tbody>
                                                                                <!-- Content -->
                                                                                <tr>
                                                                                    <td>
                                                                                        <table width="150" align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidthinner">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td style="font-family: Helvetica, arial, sans-serif; font-size: 16px; color: #333333;line-height:24px; text-align:right;"
                                                                                                        st-title="2coltitle2">
                                                                                                        {$customer_info}
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <!-- end of Content -->
                                                                                <!-- end of content -->
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <!-- end of text content table -->
                                                            </tbody>
                                                        </table>
                                                        <!-- end of right column -->
                                                    </td>
                                                </tr>
                                                <!-- Spacing -->
                                                <!--<tr>
                                 <td width="100%%" height="10"></td>
                              </tr>-->
                                                <!-- Spacing -->
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td>
                        <table width="450" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
                            <tbody>
                                <tr>
                                    <td align="center" height="10" style="font-size:1px; line-height:1px;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td width="550" align="center" height="1" bgcolor="#d1d1d1" style="font-size:1px; line-height:1px;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="center" height="10" style="font-size:1px; line-height:1px;">&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td>
                        <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                            <tbody>
                                <tr>
                                    <td width="100%%">
                                        <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                                            <tbody>
                                                <!-- Spacing -->
                                                <tr>
                                                    <td height="20" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                                                </tr>
                                                <!-- Spacing -->
                                                <tr>
                                                    <td>
                                                        <table width="560" align="center" cellpadding="0" cellspacing="0" border="0" class="devicewidthinner">
                                                            <tbody>
                                                                <!-- Title -->
                                                                <tr>
                                                                    <td style="font-family: Helvetica, arial, sans-serif; font-size: 30px; color: #333333; text-align:center; line-height: 30px;"
                                                                        st-title="fulltext-title">
                                                                        <button class="btn"><a style="color:inherit;" href="{$redirect_address}" target="_blank">Перейти в Админ-панель</button>
                                                                    </td>
                                                                </tr>
                                                                <!-- End of Title -->
                                                                <!-- End of content -->
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <!-- Spacing -->
                                                <tr>
                                                    <td height="50" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                                                </tr>
                                                <!-- Spacing -->
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>

            </tbody>
        </table>
DETAILS;


$body = <<<TAG
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Simples-Minimalistic Responsive Template</title>
    <style type="text/css">
        /* Client-specific Styles */
        
        #outlook a {
            padding: 0;
        }
        /* Force Outlook to provide a "view in browser" menu link. */
        
        body {
            width: 100% !important;
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
            margin: 0;
            padding: 0;
        }
        /* Prevent Webkit and Windows Mobile platforms from changing default font sizes, while not breaking desktop design. */
        
        .ExternalClass {
            width: 100%;
        }
        /* Force Hotmail to display emails at full width */
        
        .ExternalClass,
        .ExternalClass p,
        .ExternalClass span,
        .ExternalClass font,
        .ExternalClass td,
        .ExternalClass div {
            line-height: 100%;
        }
        /* Force Hotmail to display normal line spacing.*/
        
        #backgroundTable {
            margin: 0;
            padding: 0;
            width: 100% !important;
            line-height: 100% !important;
        }
        
        img {
            outline: none;
            text-decoration: none;
            border: none;
            -ms-interpolation-mode: bicubic;
        }
        
        a img {
            border: none;
        }
        
        .image_fix {
            display: block;
        }
        
        p {
            margin: 0px 0px !important;
        }
        
        table td {
            border-collapse: collapse;
        }
        
        table {
            border-collapse: collapse;
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }
        
        a {
            color: #0a8cce;
            text-decoration: none;
            text-decoration: none!important;
        }
        /*STYLES*/
        
        table[class=full] {
            width: 100%;
            clear: both;
        }
        /*IPAD STYLES*/
        
        @media only screen and (max-width: 640px) {
            a[href^="tel"],
            a[href^="sms"] {
                text-decoration: none;
                color: #0a8cce;
                /* or whatever your want */
                pointer-events: none;
                cursor: default;
            }
            .mobile_link a[href^="tel"],
            .mobile_link a[href^="sms"] {
                text-decoration: default;
                color: #0a8cce !important;
                pointer-events: auto;
                cursor: default;
            }
            table[class=devicewidth] {
                width: 440px!important;
                text-align: center!important;
            }
            table[class=devicewidthinner] {
                width: 420px!important;
                text-align: center!important;
            }
            img[class=banner] {
                width: 440px!important;
                height: 220px!important;
            }
            img[class=colimg2] {
                width: 440px!important;
                height: 220px!important;
            }
        }
        /*IPHONE STYLES*/
        
        @media only screen and (max-width: 480px) {
            a[href^="tel"],
            a[href^="sms"] {
                text-decoration: none;
                color: #0a8cce;
                /* or whatever your want */
                pointer-events: none;
                cursor: default;
            }
            .mobile_link a[href^="tel"],
            .mobile_link a[href^="sms"] {
                text-decoration: default;
                color: #0a8cce !important;
                pointer-events: auto;
                cursor: default;
            }
            table[class=devicewidth] {
                width: 280px!important;
                text-align: center!important;
            }
            table[class=devicewidthinner] {
                width: 260px!important;
                text-align: center!important;
            }
            img[class=banner] {
                width: 280px!important;
                height: 140px!important;
            }
            img[class=colimg2] {
                width: 280px!important;
                height: 140px!important;
            }
            td[class=mobile-hide] {
                display: none!important;
            }
            td[class="padding-bottom25"] {
                padding-bottom: 25px!important;
            }
        }
        /*my styles*/
        
        .btn {
            color: #fff;
            background-color: #0275d8;
            border-color: #0275d8;
            display: inline-block;
            font-weight: 400;
            line-height: 1.25;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            border: 1px solid transparent;
            padding: .5rem 1rem;
            font-size: 1rem;
            border-radius: .25rem;
            -webkit-transition: all .2s ease-in-out;
            -o-transition: all .2s ease-in-out;
            transition: all .2s ease-in-out;
        }
        .btn:hover {
            cursor: pointer;
            color: #fff;
            background-color: #025aa5;
            border-color: #01549b;
        }

    </style>
</head>

<body style="background-color: #f6f6f6;">
    <!-- Start of header -->
    <!-- End of Header -->
    <!-- Start of main-banner -->
    <!-- End of main-banner -->
    <!-- Start of seperator -->
    <table width="100%" bgcolor="#f6f6f6" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="seperator">
        <tbody>
            <tr>
                <td>
                    <table width="600" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
                        <tbody>
                            <tr>
                                <td align="center" height="20" style="font-size:1px; line-height:1px;">&nbsp;</td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
TAG;


$body .= <<<TAG
<div style="background-color: #f6f6f6;width: 600px;margin: auto;">
        <table width="100%" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="full-text">
            <tbody>
                <tr>
                    <td>
                        <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                            <tbody>
                                <tr>
                                    <td width="100%">
                                        <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                                            <tbody>
                                                <!-- Spacing -->
                                                <tr>
                                                    <td height="20" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                                                </tr>
                                                <!-- Spacing -->
                                                <tr>
                                                    <td>
                                                        <table width="560" align="center" cellpadding="0" cellspacing="0" border="0" class="devicewidthinner">
                                                            <tbody>
                                                                <!-- Title -->
                                                                <tr>
                                                                    <td style="font-family: Helvetica, arial, sans-serif; font-size: 30px; color: #333333; text-align:center; line-height: 30px;"
                                                                        st-title="fulltext-heading">
                                                                        Новый заказ от {$botName}!
                                                                        <br><br>
                                                                        <strong>Номер заказа #{$order_number}</strong>
                                                                    </td>
                                                                </tr>
                                                                <!-- End of Title -->
                                                                <!-- spacing -->
                                                                <tr>
                                                                    <td width="100%" height="20" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                                                                </tr>
                                                                <!-- End of spacing -->
                                                                <!-- content -->
                                                                <tr>
                                                                    <td width="80%" style="font-family: Helvetica, arial, sans-serif; font-size: 14px; color: rgba(102, 102, 102, 0.57); text-align:center; line-height: 30px;"
                                                                        st-content="fulltext-content">
                                                                        {$dateFormatted}
                                                                    </td>
                                                                </tr>
                                                                <!-- End of content -->
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <!-- Spacing -->
                                                <!--<tr>
                                 <td height="20" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                              </tr>-->
                                                <!-- Spacing -->
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
        <!-- end of full text -->
        <!-- Start of seperator -->
        <table width="100%" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="seperator">
            <tbody>
                <tr>
                    <td>
                        <table width="450" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
                            <tbody>
                                <tr>
                                    <td width="550" align="center" height="1" bgcolor="#d1d1d1" style="font-size:1px; line-height:1px;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="center" height="10" style="font-size:1px; line-height:1px;">&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
        <!-- End of seperator -->
        <!-- 2columns -->
        <table width="100%" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="2columns">
            <tbody>
TAG;

$n = count($order_names);

$bot_lead_note = "";

for ($i = 0; $i < $n; $i++) {
    $bot_lead_note .= $order_names[$i] . " X " . $order_quantities[$i] . "\n";
    $body .= sprintf($product, $order_names[$i], $order_quantities[$i], $order_quantities[$i] * $order_prices[$i]);
    if ($i != $n - 1) {
        $body .= $seperator;
    }
}

$body .= $seperatorBold;
$body .= sprintf($total, $order_total);
$body .= $seperatorBold;
$body .= $details;


$body .= <<<ENDOFPRODUCTS
<!-- Start of Postfooter -->
        <table width="100%" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="postfooter">
            <tbody>
                <tr>
                    <td>
                        <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                            <tbody>
                                <tr>
                                    <td width="100%">
                                        <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                                            <tbody>
                                                <tr>
                                                    <td align="center" valign="middle" style="font-family: Helvetica, arial, sans-serif; font-size: 14px;color: #666666" st-content="postfooter">
                                                        Что-то не правильно? <a href="http://chatbots.siera.kz" target="_blank" style="text-decoration: none; color: #0a8cce">Свяжитесь с нами</a>                                                        и мы исправим всё
                                                    </td>
                                                </tr>
                                                <!-- Spacing -->
                                                <tr>
                                                    <td width="100%" height="20"></td>
                                                </tr>
                                                <tr>
                                                    <td bgcolor="#f6f6f6" width="100%" height="20" style="font-family: Helvetica, arial, sans-serif; font-size: 13px;color: rgba(0, 0, 0, 0.54); text-align: center; padding-top:10px;padding-bottom:10px;">
                                                        Siera Coproration, ул. Маметова 67б, 2-ой этаж, офис 8
                                                        <br>
                                                        info@siera.kz
                                                    </td>
                                                </tr>
                                                <!-- Spacing -->
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
ENDOFPRODUCTS;


$body .= <<<END
</div>
    <!-- End of postfooter -->
</body>

</html>
END;

//echo $body;

$subject = "Новый заказ от " . $botName; // Choose a custom subject (not mandatory)

// $body = wordwrap($body, 70, "\r\n");

$from = $botName . " <koreanponybot@koreanpony.kz>"; // Replace "Beetle Template" with your site name (not mandatory)

$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type: text/html; charset=iso-8859-1" . "\r\n";
$headers .= "From: " . $from . "\r\n" .
    "Reply-To: koreanponybot@koreanpony.kz" . "\r\n" .
    // "Bcc: aibolikmarketing@gmail.com" . "\r\n" .
    "X-Mailer: PHP/" . phpversion();

if ($botName != '' && $to != '') {
    if (mail($to, $subject, $body, $headers)) {
        echo '<p style="color:#66A325;">Thanks! Your message has been sent.</p>';
    } else {
        echo '<p style="color:#F84B3C;">Something went wrong, go back and try again!</p>';
    }
} else {
    echo '<p style="color:#F84B3C;">You need to fill in all required fields!</p>';
}

$api = new Handler('bsolutions', 'kairkanuly@gmail.com');

$tags = array();
array_push($tags, 'заявка с бота');
array_push($tags, 'KoreanPonyBot');

$lead = new Lead();

$lead
    ->setName($customer_name . " - koreanpony.kz")
    ->setResponsibleUserId(1335936)// Aibol Kussain
    ->setStatusId(14041287) // Первичный контакт
    ->setCustomField($api->config["AddressBot"], $customer_address)
    ->setTags($tags);

commit($lead, $api);

$lead_id = $api->last_insert_id;

$note = new Note();
$note->setElementId($lead_id);
$note->setElementType(Note::TYPE_LEAD);
$note->setNoteType(Note::COMMON);
$note->setText($bot_lead_note);
commit($note, $api);

$phone = formatPhone($customer_phone);

$contact = searchContact($api, $phone);


if (!$contact) {
    $contact = new Contact();
    $contact_phone = "8" . $phone;
    $contact
        ->setName($customer_name)
        ->setResponsibleUserId(1335936)
        ->setCustomField($api->config["ContactPhone"], $customer_phone, 456446)
        ->setLinkedLeadsId($lead_id);
    commit($contact, $api);
} else {
    $updated_contact = new Contact();
    $updated_contact
        ->setName($contact->name)
        ->setUpdate($contact->id, $contact->last_modified + 1)
        ->setResponsibleUserId($contact->responsible_user_id)
        ->setLinkedLeadsId($contact->linked_leads_id);
    $updated_contact->setLinkedLeadsId($lead_id);
    commit($updated_contact, $api);
}


?>