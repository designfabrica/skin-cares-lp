<!DOCTYPE html>
<html>
<head>
    <title>Душевая лейка, очищающий фильтр для душа SKIN CARES - Korean Pony</title>
    <!-- META TAGS -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">
    <meta name="description"
          content="Skin Cares - очищающая душевая лейка и фильтр для душа, душевые насадки от Korean Pony. Очищает воду, ухаживает за кожой, экономит воду, легко устанавливается">
    <meta name="keywords"
          content="фильтры для душа, душевые насадки Skin Cares - Korean Pony, очищающая душевая лейка и фильтр для душа, очищает воду, ухаживает за кожой, экономит воду, легко устанавливается">

    <meta property="og:title" content="Душевая лейка, очищающий фильтр для душа SKIN CARES - Korean Pony" />
    <meta property="og:description" content="Skin Cares - очищающая душевая лейка и фильтр для душа, душевые насадки от Korean Pony. Очищает воду, ухаживает за кожой, экономит воду, легко устанавливается">
    <meta property="og:type" content="website" />
    <meta property="og:url" content="http://www.koeranpony.kz" />
    <meta property="og:image" content="http://koreanpony.kz/assets/img/site-hero.jpg" />

    <!--    Fonts-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,800|Open+Sans:400,600,700,800"
          rel="stylesheet">

    <!--Sweet Alert styles-->
    <link rel="stylesheet" type="text/css" href="assets/swal/sweetalert.css">
    <!-- STYLES -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/ionicons.min.css">
    <link rel="stylesheet" href="assets/css/flexslider.css">
    <link rel="stylesheet" href="assets/css/animate.css">
    <link rel="stylesheet" href="assets/css/style.css">

    <?php include_once("blocks/analyticstracking.php") ?>

    <!--    Source -->
    <script>
        <?php
        $source = "direct";
        if (isset($_GET["from"])) {
            $source = $_GET["from"];
            if ($source == "instagram") {
                $source = "instagram_profile";
            }
        } else if (isset($_GET["utm_source"])) {
            $source = $_GET["utm_source"];
            $source .= ";" . $_GET["utm_campaign"];
            $source .= ";" . $_GET["utm_medium"];
            if (isset($_GET["utm_content"])) {
                $source .= ";" . $_GET["utm_content"];
            }
        }
        ?>
        var source = "<?php echo $source ?>";
    </script>
</head>
<body>
<!-- HEADER  -->
<header class="main-header">
    <div class="container">
        <div class="logo">
            <a href="index.php"><img alt="душевые насадки Skin Cares Korean Pony" src="assets/img/logo_white.png" alt="logo"></a>
            <h3>KOREAN PONY<br>
                <small>Магазин Корейсих товаров</small>
            </h3>
        </div>

        <div class="menu">
            <!-- desktop navbar -->
            <nav class="desktop-nav">
                <ul class="first-level">
                    <li><a href="#main">Главная</a></li>
                    <li><a href="#features">О Skin Cares</a></li>
                    <li><a href="#installation">Установка</a></li>
                    <li><a href="#whyus">Почему мы</a></li>
                    <li><a href="#testimonials">Отзывы</a></li>
                    <li><a href="#contacts">Контакты</a></li>
                    <li class="callme">
                        <button class="btn green" data-toggle="modal" data-target="#myModal"
                                onclick="yaCounter43198614.reachGoal('phone'); return true;">Позвоните мне
                        </button>
                    </li>
                </ul>
            </nav>
            <!-- mobile navbar -->
            <nav class="mobile-nav"></nav>
            <div class="menu-icon">
                <div class="line"></div>
                <div class="line"></div>
                <div class="line"></div>
            </div>
        </div>
    </div>
</header>

<!-- HERO SECTION  -->
<div id="main" class="site-hero">
    <div class="videocontent">
        <video autoplay loop muted id="bgvideo" style="width:100%;height:100%;">
            <!-- <img src="img/overlay.png" style="position:absolute;float:left;width:100%;height:100%;background-color:#000;z-index:300000;"/> -->
            <source src="video.mp4" type="video/mp4"></source>
        </video>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-lg-7 col-sm-12">
                <!--<div><span class="small-title uppercase montserrat-text">we're</span></div>-->
                <?php
                    $offer_normal = "Очищающая насадка для душа <span>SKIN CARES<br/></span>и 3 фильтра";
                    if (isset($_GET["utm_content"]) && $_GET["utm_content"] == "offer1") {
                        $offer_normal = "Купи лейку <span>SKIN CARES,<br/></span>
                    и если не понравится - вернем <span>120%</span> денег";
                    }
                ?>
                <h1 class="big-title montserrat-text uppercase"><?php echo $offer_normal; ?>
                </h1>
                <div class="price">
                    <p class="uppercase">Цена: <span class="old-price">21 900</span> <br/> <span>17 900</span> тг.</p>
                    <button type="submit" class="btn green" style="margin-top:20px" data-toggle="modal"
                            data-target="#myModal" onclick="yaCounter43198614.reachGoal('order1'); return true;"><span>Заказать</span>
                    </button>
                </div>

                <p>Инновационный товар из <span class="em">Южной Кореи</span> для красоты и здоровья, помощник в очистке
                    и экономии воды! </p>
                <p>Бесплатная доставка по Казахстану</p>
            </div>
        </div>
    </div>
</div>


<!-- WHY CHOOSE US -->
<section id="features" class="services">
    <div class="container">
        <div class="row">
            <div class="section-title">
                <h2>почему skin cares</h2>
                <h4>Очищающая душевая лейка "Skin Cares" является оригинальным продуктом из Южной Кореи, имеет все
                    необходимые сертификаты</h4>
            </div>
        </div>

        <div class="col-md-7 col-sm-12 services-left wow fadeInUp">
            <div class="row" style="margin-bottom:50px">
                <div class="col-md-6 col-sm-12">
                    <div class="row">
                        <i class="icon ion-funnel"></i>
                        <h3 class="montserrat-text uppercase service-title">Очищает воду</h3>
                        <ul>
                            <li>В водопроводных трубах со временем скапливаются вредные вещества. <strong>Душевая лейка "Skin
                                    Cares"</strong> предотвращает попадание вредных веществ на Вашу кожу.
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="row">
                        <i class="icon ion-refresh"></i>
                        <h3 class="montserrat-text uppercase service-title">Ухаживает за кожой</h3>
                        <ul>
                            <li>Система не только очистит воду от вредных примесей, но также обладает свойствами
                                ионизации воды, превращая банальную гигиеническую процедуру в СПА-уход за телом!
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <div class="row">
                        <i class="icon ion-waterdrop"></i>
                        <h3 class="montserrat-text uppercase service-title">Экономие воды на 30%</h3>
                        <ul>
                            <li>Благодаря диффузному распылению воды – вы экономите <strong>до 30% воды</strong> чем обычная
                                душевая лейка.
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="row">
                        <i class="icon ion-wrench"></i>
                        <h3 class="montserrat-text uppercase service-title">Легко устанавливается</h3>
                        <ul>
                            <li>Установка - размер посадки 1/2 дюйма! Это стандарт для 96% всех вариантов шлангов для
                                душа.
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-5 col-sm-12 services-right wow fadeInUp" data-wow-delay=".1s">
            <div class="row">
                <!--                <img src="assets/img/hero1.jpg" />-->
                <iframe height="400" src="https://www.youtube.com/embed/IpmaIobz-7A" frameborder="0"
                        allowfullscreen></iframe>
            </div>
        </div>

    </div>
</section>

<!-- INSTALLATION -->
<section id="installation" class="installation">
    <div class="container">
        <div class="row">
            <div class="section-title">
                <h2>Установка</h2>
                <h4>Установить лейку Skin Cares проще простого. Просмотрите видео и Вы сами сможете заменить свою лейку
                    на "Skin Cares"</h4>
            </div>
        </div>

        <div class="col-md-6 col-sm-12 services-right wow fadeInUp" data-wow-delay=".1s">
            <iframe height="400" src="https://www.youtube.com/embed/CuIHra0VIUY" frameborder="0"
                    allowfullscreen></iframe>
        </div>

        <div class="col-md-6 col-sm-12 services-right wow fadeInUp" data-wow-delay=".1s">
            <img alt="фильтры для душа, душевые насадки Skin Cares - Korean Pony" src="assets/img/installation.PNG">
        </div>

    </div>
</section>

<section id="whyus">
    <div class="container">
        <div class="row">
            <div class="row">
                <div class="section-title">
                    <h2>почему выбирают нас</h2>
                    <h4>Несколько причин, почему стоит брать именно у нас</h4>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp">
                <div class="benefits_1_single">
                    <i class="icon ion-android-checkmark-circle"></i>
                    <h3 class="title montserrat-text uppercase">Гарантия качества</h3>
                    <p>Очищающая душевая лейка "Skin Cares" привозится напрямую из Южной Кореи, от производителей.
                    </p>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay=".1s">
                <div class="benefits_1_single">
                    <i class="icon ion-android-people"></i>
                    <h3 class="title montserrat-text uppercase">Официальные диллеры</h3>
                    <p>Наша компания является официальным дилером "Skin Cares" в Казахстане.
                    </p>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay=".2s">
                <div class="benefits_1_single">
                    <i class="icon ion-android-car"></i>
                    <h3 class="title montserrat-text uppercase">Доставка до двери</h3>
                    <p>Мы осущеcтвляем доставку почти до любой точки в Казахстане.
                    </p>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay=".3s">
                <div class="benefits_1_single">
                    <i class="icon ion-bag"></i>
                    <h3 class="title montserrat-text uppercase">Оплата при получении</h3>
                    <p>Вы платите только тогда, когда Вы получите, проверите товар и его соответсвие описанию.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>


<section id="testimonials">
    <div class="container">
        <div class="row">
            <div class="section-title">
                <h2>Счастливые клиенты</h2>
                <h4>Пользование "Skin Cares" никого не оставляет равнодушным</h4>
            </div>

            <div class="col-md-12 wow fadeInUp">
                <div id="carousel-example-caption-testimonial" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-example-caption-testimonial" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-example-caption-testimonial" data-slide-to="1"></li>
                        <li data-target="#carousel-example-caption-testimonial" data-slide-to="2"></li>
                    </ol>
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <div class="item active">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <!-- IMAGE -->
                                        <img src="assets/img/client1.jpg" alt="">
                                        <div class="testimonial_caption">
                                            <!-- DESCRIPTION -->
                                            <p>Необходимая вещь в каждом доме, особенно актуально тем у кого маленькие детки!</p>
                                            <h2>Нургуль Жумаева</h2>
                                            <h4><span>г. Астана</span></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <!-- IMAGE -->
                                        <img src="assets/img/client2.jpg" alt="">
                                        <div class="testimonial_caption">
                                            <!-- DESCRIPTION -->
                                            <p>Отличная лейка. Взяли с мужем недавно, остались довольны. Хорошо подает воду даже при
                                                низком напоре. Сначала было непривычно, как смешивалась горячая и холодная водичка,
                                                пришлось немного приноровиться и выкручивать больше горячий кран, но это мелочи. Через
                                                пару недель становится отчетливо видно по фильтру, какую пакость мы на себя выливали до
                                                этой покупки. Спасибо :)</p>
                                            <h2>Бахыт Махметова</h2>
                                            <h4><span>г. Шымкент</span></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <!-- IMAGE -->
                                        <img src="assets/img/client3.jpg" alt="">
                                        <div class="testimonial_caption">
                                            <!-- DESCRIPTION -->
                                            <p>Спасибо Korean Pony. Эта насадка никого не оставит. Вы даже не
                                                представляете какая вода течет из наших кранов.</p>
                                            <h2>Назгуль Дуйсенбаева</h2>
                                            <h4><span>г. Алматы</span></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<!-- newsletter -->
<section class="green-section wow fadeInUp offer" style="padding:50px 0">
    <div class="container">
        <div class="col-md-6 col-sm-12">
            <div class="row">
                <div class="big-title montserrat-text white-text uppercase" style="font-size: 36px;">
                    Готовы перейти на чистую воду?
                </div>
                <p class="white-text">Закажите очищающую душевую лейку "Skin Cares" сейчас, и получите 3 картриджа в
                    подарок!</p>
                <div class="price white-text">
                    <p class="uppercase white-text">Всего за <br/> <span>17 900</span> тг.</p>
                    <button type="submit" class="btn white" style="margin-top:20px" data-toggle="modal"
                            data-target="#myModal" onclick="yaCounter43198614.reachGoal('order2'); return true;"><span>Заказать</span>
                    </button>
                </div>
                <p class="white-text">Бесплатная доставка по Казахстану</p>
            </div>
        </div>
    </div>
</section>

<section class="delivery">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <!--<img src="assets/img/kzmap.png"/>-->
                <h4 class="uppercase montserrat-text">Доставка до любой точки в Казахстане<br/> <span>БЕСПЛАТНО</span>
                </h4>
                <div class="icons">
                    <div class="iconitem">
                        <i class="ion-android-car"></i>
                        <!--<div class="title montserrat-text uppercase">keep pulse going</div>-->
                    </div>
                    <div class="iconitem">
                        <i class="icon ion-android-plane"></i>
                        <!--<div class="title montserrat-text uppercase">keep pulse going</div>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- FOOTER -->
<footer class="main-footer wow fadeInUp" id="contacts">
    <div class="container">
        <div class="col-md-8 col-sm-12">
            <div class="row">
                <div class="logo">
                    <a href="index.php"><img alt="фильтры для душа, душевые насадки Skin Cares - Korean Pony" src="assets/img/logo.png" alt="logo"></a>
                    <h3>KOREAN PONY<br>
                        <small>Магазин Корейсих товаров</small>
                    </h3>
                </div>
                <a class="public_offer" target="_blank"
                   href="https://docs.google.com/document/d/1ZvZIH98ocytxa7MbRuDSsGjLzbut9lhLxS6SD-Aphho/edit?usp=sharing">Публичная
                    оферта</a>
            </div>
        </div>

        <div class="col-md-4 col-sm-12" style="text-align:right">
            <div class="row">
                <div class="uppercase gray-text">
                    <p class="copyright">Skin Cares &copy; Сделано с <span class="love_icon"></span> студией Design
                        Fabrica</p>
                </div>
                <a class="phone" href="tel:+77024246492">+7(702)424 64 92</a>
                <br/>
                <ul class="social-icons clearfix" style="margin-top:30px;float:right">
                    <li><a href="https://www.facebook.com/koreanponykz" target="_blank"><i
                                    class="icon ion-social-facebook"></i></a></li>
                    <!--<li><a href="#" target="_blank"><i class="icon ion-social-twitter"></i></a></li>-->
                    <!--<li><a href="#" target="_blank"><i class="icon ion-social-youtube"></i></a></li>-->
                    <!--<li><a href="#" target="_blank"><i class="icon ion-social-linkedin"></i></a></li>-->
                    <!--<li><a href="#" target="_blank"><i class="icon ion-social-pinterest"></i></a></li>-->
                    <li><a href="http://instagram.com/korean_pony_kz" target="_blank"><i
                                    class="icon ion-social-instagram"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>

<div id="myModal" class="modal fade" role="dialog">

    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <!--<script>var amo_forms_params = {id: 141928, hash: 'e9e0fe49f9926ac39838dbd8c27415d5', locale: 'ru'};</script><script id="amoforms_script" async="async" src="https://forms.amocrm.ru/forms/assets/js/amoforms.js"></script>-->
                <h4 class="modal-title">Оставить заявку</h4>
                <form>
                    <div class="form-group-div">
                        <input type="text" id="form_name" placeholder="Имя*" name="name">
                    </div>
                    <div class="form-group-div">
                        <input type="text" id="form_phone" placeholder="Номер*" name="number">
                    </div>
                    <input id="submit_form" type="submit" class="btn green" value="Отправить"
                           onclick="yaCounter43198614.reachGoal('submit'); fbq('track', 'Lead', {value: 17900.00,currency: 'KZT'});return true;">
                </form>
            </div>
        </div>

    </div>
</div>

<!-- SCRIPTS -->
<script type="text/javascript" src="assets/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.flexslider.js"></script>
<script type="text/javascript" src="assets/js/smoothScroll.js"></script>
<script type="text/javascript" src="assets/js/wow.min.js"></script>
<script type="text/javascript" src="assets/swal/sweetalert.min.js"></script>
<script type="text/javascript" src="assets/js/main.js"></script>

<script type="text/javascript" charset="utf-8">
    $(window).load(function () {
        new WOW().init();


        // initialize isotope
        var $container = $('.portfolio_container');
        $container.isotope({
            filter: '*',
        });

        $(".testimonials").flexslider({
            directionNav: false,
            controlNav: false,
            smoothHeight: true,
        });

        $("#test1").click(function () {
            $(".testimonials").flexslider(0);
        });
        $("#test2").click(function () {
            $(".testimonials").flexslider(1);
        });
        $("#test3").click(function () {
            $(".testimonials").flexslider(2);
        });

        $("a").on('click', function (event) {

            // Make sure this.hash has a value before overriding default behavior
            if (this.hash !== "") {
                // Prevent default anchor click behavior
                event.preventDefault();

                // Store hash
                var hash = this.hash;

                // Using jQuery's animate() method to add smooth page scroll
                // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
                $('html, body').animate({
                    scrollTop: $(hash).offset().top
                }, 800, function () {

                    // Add hash (#) to URL when done scrolling (default click behavior)
                    window.location.hash = hash;
                });
            } // End if
        });
    });
</script>

<script type="text/javascript" src="//cdn.callbackhunter.com/cbh.js?hunter_code=888905ad5f16c38a620ca96dbf121bea"
        charset="UTF-8"></script>
</body>
</html>