<?php
return [
    'ResponsibleUserId' => 330242, //ID ответственного менеджера
    'LeadStatusId' => 8156376, // ID первого статуса сделки
    'ContactFieldPhone' => 1426544, // ID поля номера телефона
    'ContactFieldEmail' => 1426546, // ID поля емейла
    'LeadFieldCustom' => 1740351, // ID кастомного поля сделки
    'LeadFieldCustomValue1' => 4055517, // ID первого значения кастомного поля сделки
    'LeadFieldCustomValue2' => 4055519, // ID второго значения кастомного поля сделки
    'InstallPlannedDate' => 294419, // ID дата планируемой установки
    'InstallPlanDays' => 294433, // ID срок планируемой установки
    'SumOfGoods' => 280391, // ID сумма приложении
    'SumOfPrepaidGoods' => 306609, // ID сумма предоплаты
    'DogovorNo' => 306661, // ID Договор номер
    'ContactDogovor' => 366573, // Договор, которые присвоен контакту
    'DogovorDate' => 368487, // Дата договора, который привязан контакту
    'PrilojeniyeNo' => 306663, // ID приложения номер
    'PrihodnikNo' => 306681, // ID приходный кассовый ордер
    'PrihodnikOtData' => 306695, // ПКО от, дата
    'OrderNo' => 306679, // ID номер заказа
    'Phone' => 279491, // ID номер телефона
    'Izdeliya' => 280369, // Чего хочет ЛИД(изделия)
    'ObjectAddress' => 280433, // Адрес объекта
    'Zamer3d' => 294461, // 3D Замер
    'PicturesOfInspiration' => 294465, // Рисунки вдохновения
    'ZamerPhotos' => 294467, // Фото с замера
    'ExcelSmeta' => 294469, // Смета в Excel
    'OprosnyiListExcel' => 294471, // Опросный лист в Excel

    'KuhnyaPrice' => 370087,
    'KuhnyaDate' => 370089,

    'ShkafPrice' => 370091,
    'ShkafDate' => 370093,

    'GarderobPrice' => 370095,
    'GarderobDate' => 370097,

    'MebelOfisPrice' => 370099,
    'MebelOfisDate' => 370101,

    'TvTumbaPrice' => 370103,
    'TvTumbaDate' => 370105,

    'DetskayaPrice' => 370107,
    'DetskayaDate' => 370109,

    'KrovatPrice' => 370115,
    'KrovatDate' => 370155,

    'PrihozhayaPrice' => 370157,
    'PrihozhayaDate' => 370159,

    'TumbochkiPrice' => 370161,
    'TumbochkiDate' => 370163,

    'TruymoPrice' => 370165,
    'TruymoDate' => 370167

];
?>