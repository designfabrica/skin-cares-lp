<?php

use \AmoCRM\Handler;
use \AmoCRM\Request;

require('vendor/autoload.php');
//require_once __DIR__ . '/vendor/autoload.php';

/* Создание экземпляра API, где "domain" - имя вашего домена в AmoCRM, а
"user@example.com" - email пользователя, от чьего имени будут совершаться запросы */
$api = new Handler('whitecube2', 'aibolikdev@gmail.com');

/* Создание экземляра запроса */

/* Вторым параметром можно передать дополнительные параметры поиска (смотрите в документации)
В этом примере мы ищем пользователя с номером телефона +7 916 111-11-11
Чтобы получить полный список, укажите пустой массив []
Третьим параметром указывается метод в формате [название объекта, метод] */
$request_get = new Request(Request::GET, ['id' => '2191697'], ['leads', 'list']);

/* Выполнение запроса */
$result = $api->request($request_get)->result;

//print_r($result);

$custom_fields = $result->leads[0]->custom_fields;

$installPlannedDate = "";
$installPlanDays = "";
$sumOfGoods = "";
$sumOfPrepaidGoods = "";
$dogovorNo = "";
$prilojeniyeNo = "";
$dogovorDate = "";
$prihodnikNo = "";
$orderNo = "";

for($i = 0; $i < count($custom_fields); $i++) {
    switch ($custom_fields[$i]->id) {
        case $api->config["InstallPlannedDate"]:
            $installPlannedDate = $custom_fields[$i]->values[0]->value;
            break;
        case $api->config["InstallPlanDays"]:
            $installPlanDays = $custom_fields[$i]->values[0]->value;
            break;
        case $api->config["SumOfGoods"]:
            $sumOfGoods = $custom_fields[$i]->values[0]->value;
            break;
        case $api->config["SumOfPrepaidGoods"]:
            $sumOfPrepaidGoods = $custom_fields[$i]->values[0]->value;
            break;
        case $api->config["DogovorNo"]:
            $dogovorNo = $custom_fields[$i]->values[0]->value;
            break;
        case $api->config["PrilojeniyeNo"]:
            $prilojeniyeNo = $custom_fields[$i]->values[0]->value;
            break;
        case $api->config["DogovorDate"]:
            $dogovorDate = $custom_fields[$i]->values[0]->value;
            break;
        case $api->config["PrihodnikNo"]:
            $prihodnikNo = $custom_fields[$i]->values[0]->value;
            break;
        case $api->config["OrderNo"]:
            $orderNo = $custom_fields[$i]->values[0]->value;
            break;
    }
}

print_r($installPlannedDate);
print("<br />");
print_r($installPlanDays);
print("<br />");
print_r($sumOfGoods);
print("<br />");
print_r($sumOfPrepaidGoods);
print("<br />");
print_r($dogovorNo);
print("<br />");
print_r($prilojeniyeNo);
print("<br />");
print_r($dogovorDate);
print("<br />");
print_r($prihodnikNo);
print("<br />");
print_r($orderNo);
print("<br />");


/* Результат запроса сохраняется в свойстве "result" объекта \AmoCRM\Handler()
Содержит в себе объект, полученный от AmoCRM, какой конкретно - сверяйтесь с документацией для каждого метода
Ошибка запроса выбросит исключение */
//$api->result == false, если ответ пустой (то есть контакты с таким телефоном не найдены) */

?>
