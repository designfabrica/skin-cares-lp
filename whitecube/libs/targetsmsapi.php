<?php

include_once 'Curl.php';

class TargetSMS
{

//    private $login = "Whitecube";
//    private $pass = "12345";
    protected $curl;
    private $apiUrl = "https://sms.targetsms.ru/sendsmsjson.php";

    function __construct()
    {
        $this->curl = new Curl\Curl();
    }

    /**
     * @return string
     */
    public function getApiUrl()
    {
        return $this->apiUrl;
    }


    public function sendSMS($content, $phone)
    {
        $param = array(
            'security' => array('login' => 'Whitecube', 'password' => '12345'),
            'type' => 'sms',
            'message' => array(
                array(
                    'type' => 'sms',
                    'sender' => 'whitecube',
                    'text' => $content,
                    'abonent' => array(
                        array('phone' => $phone, 'number_sms' => '1')
                    )
                )
            )
        );

        try {
            $response = $this->curl->post($this->getApiUrl(), json_encode($param));

            return $response;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }


    }
}

?>