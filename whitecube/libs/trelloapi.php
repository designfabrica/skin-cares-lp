<?php

include_once 'Curl.php';

class Trello
{

    private $key = "feb797aa9f1cf3785fa1c26dcac1fb5f";
    private $token = "4990eb263d5fd860d767f1d9c7b939b12fa89744a6f686cd4c7e83fa24a6cb93";
    private $apiUrl = 'https://api.trello.com/';
    private $apiVersion = '1';

    protected $curl;

    function __construct(){
        $this->curl = new Curl\Curl();
    }

    /**
     * Mount queryString for API Calls
     * @return string QueryString for the calls
     */
    protected function getAuthParam(){
        return '?key=' . $this->key . '&token=' . $this->token;
    }

    /**
     * Mount the URL for the Trello APO
     * @return string URL for the API
     */
    protected function getUrl(){
        return $this->apiUrl . $this->apiVersion . '/';
    }

    /**
     *  Does a PUT request to Trello API
     * @param  string $listId ID of item to be putted
     * @param  array $data         	Data to be putted
     * @param  string $complement   Complement to the API. For example, the action
     * @return object               The response from Trello API
     */
    public function createCard($data, $complement = '') {
        try {
            $authParam = $this->getAuthParam();
            $url = $this->getUrl();

            $rs = $this->curl->post($url . "cards" .'/' . $complement . $authParam, $data);
            return $this->curl->response;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

}

?>