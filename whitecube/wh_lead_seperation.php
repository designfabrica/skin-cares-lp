<?php

use AmoCRM\Contact;
use AmoCRM\Handler;
use AmoCRM\Lead;
use AmoCRM\Request;

require('libs/amoapi.php');

$api = new Handler('whitecube2', 'aibolikdev@gmail.com');

$lead_id = "";

if (isset($_POST)) {
    $lead_id = $_POST["leads"]["status"][0]["id"];
} else {
    exit("Invalid leadId");
}

//$lead_id = "2191697";

$lead = getLead($lead_id, $api);
$contact = getContact($lead->main_contact_id, $api);

$num_of_leads = count($contact->linked_leads_id);
$prilojeniye = $num_of_leads + 1;

$lead_custom_fields = $lead->custom_fields;
$fields = array();
for ($i = 0; $i < count($lead_custom_fields); $i++) {
    switch ($lead_custom_fields[$i]->id) {
        case $api->config["SumOfGoods"]:
            $sum_of_goods = $lead_custom_fields[$i]->values[0]->value;
            $fields[$api->config["SumOfGoods"]] = $sum_of_goods;
            break;
        case $api->config["InstallPlannedDate"]:
            $install_planned_date = $lead_custom_fields[$i]->values[0]->value;
            $fields[$api->config["InstallPlannedDate"]] = $install_planned_date;
            break;
        case $api->config["InstallPlanDays"]:
            $install_plan_days = $lead_custom_fields[$i]->values[0]->value;
            $fields[$api->config["InstallPlanDays"]] = $install_plan_days;
            break;
        case $api->config["Zamer3d"]:
            $zamer_3d = $lead_custom_fields[$i]->values[0]->value;
            $fields[$api->config["Zamer3d"]] = $zamer_3d;
            break;
        case $api->config["PicturesOfInspiration"]:
            $pictures_of_inspiration = $lead_custom_fields[$i]->values[0]->value;
            $fields[$api->config["PicturesOfInspiration"]] = $pictures_of_inspiration;
            break;
        case $api->config["ZamerPhotos"]:
            $zamer_photos = $lead_custom_fields[$i]->values[0]->value;
            $fields[$api->config["ZamerPhotos"]] = $zamer_photos;
            break;
        case $api->config["ExcelSmeta"]:
            $excel_smeta = $lead_custom_fields[$i]->values[0]->value;
            $fields[$api->config["ExcelSmeta"]] = $excel_smeta;
            break;
        case $api->config["OprosnyiListExcel"]:
            $oprosnyi_list_excel = $lead_custom_fields[$i]->values[0]->value;
            $fields[$api->config["OprosnyiListExcel"]] = $oprosnyi_list_excel;
            break;
        case $api->config["PrihodnikNo"]:
            $prihodnik_no = $lead_custom_fields[$i]->values[0]->value;
            $fields[$api->config["PrihodnikNo"]] = $prihodnik_no;
            break;
        case $api->config["PrihodnikOtData"]:
            $prihodnik_ot_data = $lead_custom_fields[$i]->values[0]->value;
            $fields[$api->config["PrihodnikOtData"]] = $prihodnik_ot_data;
            break;
        case $api->config["SumOfPrepaidGoods"]:
            $sum_of_prepaid_goods = $lead_custom_fields[$i]->values[0]->value;
            $fields[$api->config["SumOfPrepaidGoods"]] = $sum_of_prepaid_goods;
            break;
        case $api->config["OrderNo"]:
            $order_no = $lead_custom_fields[$i]->values[0]->value;
            $fields[$api->config["OrderNo"]] = $order_no;
            break;
        case $api->config["Izdeliya"]:
            $izdeliya = $lead_custom_fields[$i]->values;
            break;
    }
}

if (!isset($izdeliya)) {
    exit("Нет изделии");
}

if(count($izdeliya) <= 1) {
    exit("Одно изделие, ничего не делать");
}

$izdeliya_details = array();

foreach ($izdeliya as $izdeliye) {
    $izdeliya_details[$izdeliye->enum] = array();
}

for ($i = 0; $i < count($lead_custom_fields); $i++) {
    switch ($lead_custom_fields[$i]->id) {
        case $api->config["KuhnyaPrice"]:
            $izdeliya_details["634383"]["price"] = $lead_custom_fields[$i]->values[0]->value;
            $izdeliya_details["634383"]["field_price_id"] = $lead_custom_fields[$i]->id;
            break;
        case $api->config["KuhnyaDate"]:
            $izdeliya_details["634383"]["date"] = $lead_custom_fields[$i]->values[0]->value;
            $izdeliya_details["634383"]["field_date_id"] = $lead_custom_fields[$i]->id;
            break;
        case $api->config["ShkafPrice"]:
            $izdeliya_details["634385"]["price"] = $lead_custom_fields[$i]->values[0]->value;
            $izdeliya_details["634385"]["field_price_id"] = $lead_custom_fields[$i]->id;
            break;
        case $api->config["ShkafDate"]:
            $izdeliya_details["634385"]["date"] = $lead_custom_fields[$i]->values[0]->value;
            $izdeliya_details["634385"]["field_date_id"] = $lead_custom_fields[$i]->id;
            break;
        case $api->config["GarderobPrice"]:
            $izdeliya_details["634387"]["price"] = $lead_custom_fields[$i]->values[0]->value;
            $izdeliya_details["634387"]["field_price_id"] = $lead_custom_fields[$i]->id;
            break;
        case $api->config["GarderobDate"]:
            $izdeliya_details["634387"]["date"] = $lead_custom_fields[$i]->values[0]->value;
            $izdeliya_details["634387"]["field_date_id"] = $lead_custom_fields[$i]->id;
            break;
        case $api->config["MebelOfisPrice"]:
            $izdeliya_details["634389"]["price"] = $lead_custom_fields[$i]->values[0]->value;
            $izdeliya_details["634389"]["field_price_id"] = $lead_custom_fields[$i]->id;
            break;
        case $api->config["MebelOfisDate"]:
            $izdeliya_details["634389"]["date"] = $lead_custom_fields[$i]->values[0]->value;
            $izdeliya_details["634389"]["field_date_id"] = $lead_custom_fields[$i]->id;
            break;
        case $api->config["TvTumbaPrice"]:
            $izdeliya_details["634391"]["price"] = $lead_custom_fields[$i]->values[0]->value;
            $izdeliya_details["634391"]["field_price_id"] = $lead_custom_fields[$i]->id;
            break;
        case $api->config["TvTumbaDate"]:
            $izdeliya_details["634391"]["date"] = $lead_custom_fields[$i]->values[0]->value;
            $izdeliya_details["634391"]["field_date_id"] = $lead_custom_fields[$i]->id;
            break;
        case $api->config["DetskayaPrice"]:
            $izdeliya_details["634393"]["price"] = $lead_custom_fields[$i]->values[0]->value;
            $izdeliya_details["634393"]["field_price_id"] = $lead_custom_fields[$i]->id;
            break;
        case $api->config["DetskayaDate"]:
            $izdeliya_details["634393"]["date"] = $lead_custom_fields[$i]->values[0]->value;
            $izdeliya_details["634393"]["field_date_id"] = $lead_custom_fields[$i]->id;
            break;
        case $api->config["KrovatPrice"]:
            $izdeliya_details["634395"]["price"] = $lead_custom_fields[$i]->values[0]->value;
            $izdeliya_details["634395"]["field_price_id"] = $lead_custom_fields[$i]->id;
            break;
        case $api->config["KrovatDate"]:
            $izdeliya_details["634395"]["date"] = $lead_custom_fields[$i]->values[0]->value;
            $izdeliya_details["634395"]["field_date_id"] = $lead_custom_fields[$i]->id;
            break;
        case $api->config["PrihozhayaPrice"]:
            $izdeliya_details["836033"]["price"] = $lead_custom_fields[$i]->values[0]->value;
            $izdeliya_details["836033"]["field_price_id"] = $lead_custom_fields[$i]->id;
            break;
        case $api->config["PrihozhayaDate"]:
            $izdeliya_details["836033"]["date"] = $lead_custom_fields[$i]->values[0]->value;
            $izdeliya_details["836033"]["field_date_id"] = $lead_custom_fields[$i]->id;
            break;
        case $api->config["TumbochkiPrice"]:
            $izdeliya_details["836035"]["price"] = $lead_custom_fields[$i]->values[0]->value;
            $izdeliya_details["836035"]["field_price_id"] = $lead_custom_fields[$i]->id;
            break;
        case $api->config["TumbochkiDate"]:
            $izdeliya_details["836035"]["date"] = $lead_custom_fields[$i]->values[0]->value;
            $izdeliya_details["836035"]["field_date_id"] = $lead_custom_fields[$i]->id;
            break;
        case $api->config["TruymoPrice"]:
            $izdeliya_details["836037"]["price"] = $lead_custom_fields[$i]->values[0]->value;
            $izdeliya_details["836037"]["field_price_id"] = $lead_custom_fields[$i]->id;
            break;
        case $api->config["TruymoDate"]:
            $izdeliya_details["836037"]["date"] = $lead_custom_fields[$i]->values[0]->value;
            $izdeliya_details["836037"]["field_date_id"] = $lead_custom_fields[$i]->id;
            break;
    }
}

$lead_ids = array();

foreach ($izdeliya as $izdeliye) {
    $new_lead = new Lead();
    $new_lead
        ->setName($lead->name . " - " . $izdeliye->value)
        ->setResponsibleUserId($lead->responsible_user_id)
        ->setStatusId($lead->status_id);
    foreach ($fields as $key => $value) {
        $new_lead->setCustomField(
            $key,
            $value
        );
    }
    $new_lead->setCustomField(
        $api->config["PrilojeniyeNo"],
        $prilojeniye
    );
    $new_lead->setCustomField(
        $api->config["Izdeliya"],
        $izdeliye->value,
        $izdeliye->enum
    );

    $izdeliye_detail = $izdeliya_details[$izdeliye->enum];

    $new_lead->setCustomField(
        $izdeliye_detail["field_price_id"],
        $izdeliye_detail["price"]
    );

    $new_lead->setCustomField(
        $izdeliye_detail["field_date_id"],
        $izdeliye_detail["date"]
    );

    $new_lead->setPrice($izdeliye_detail["price"]);
    $new_lead->setCustomField(
        $api->config["InstallPlannedDate"],
        $izdeliye_detail["date"]
    );

    $prilojeniye = $prilojeniye + 1;
    $api->request(new Request(Request::SET, $new_lead));
    array_push($lead_ids, $api->last_insert_id);
}

if (!empty($lead_ids) && count($lead_ids) > 0) {
    print_r($contact->linked_leads_id);

    $new_contact = new Contact();

    $new_contact
        ->setName($contact->name)
        ->setUpdate($contact->id, $contact->last_modified + 1)
        ->setResponsibleUserId($contact->responsible_user_id)
        ->setLinkedLeadsId($lead_ids);

    print_r($new_contact->linked_leads_id);

    $api->request(new Request(Request::SET, $new_contact));
}


?>