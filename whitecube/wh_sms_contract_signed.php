<?php

use \AmoCRM\Handler;
use \AmoCRM\Request;


require('libs/amoapi.php');
require('libs/targetsmsapi.php');

/* Создание экземпляра API, где "domain" - имя вашего домена в AmoCRM, а
"user@example.com" - email пользователя, от чьего имени будут совершаться запросы */
$api = new Handler('whitecube2', 'aibolikdev@gmail.com');

/* Создание экземляра запроса */

$lead_id = "";

if (isset($_POST)) {
    $lead_id = $_POST["leads"]["status"][0]["id"];
} else {
    exit("Invalid leadId");
}

$lead = getLead($lead_id, $api);

$lead_custom_fields = $lead->custom_fields;
$contact_id = $lead->main_contact_id;

$install_planned_date = "";
$sum_of_goods = "";
$sum_of_prepaid_goods = "";
$dogovor_no = "";

for ($i = 0; $i < count($lead_custom_fields); $i++) {
    switch ($lead_custom_fields[$i]->id) {
        case $api->config["InstallPlannedDate"]:
            $install_planned_date = $lead_custom_fields[$i]->values[0]->value;
            break;
        case $api->config["SumOfGoods"]:
            $sum_of_goods = $lead_custom_fields[$i]->values[0]->value;
            break;
        case $api->config["SumOfPrepaidGoods"]:
            $sum_of_prepaid_goods = $lead_custom_fields[$i]->values[0]->value;
            break;
    }
}

$date = date_create($install_planned_date);
$formatted_planned_date = date_format($date, 'Y/m/d');

$contact = getContact($lead->main_contact_id, $api);
$contact_custom_fields = $contact->custom_fields;

$contact_name = $contact->name;
$contact_phone = "";

for ($i = 0; $i < count($contact_custom_fields); $i++) {
    switch ($contact_custom_fields[$i]->id) {
        case $api->config["Phone"]:
            $contact_phone = $contact_custom_fields[$i]->values[0]->value;
            break;
        case $api->config["ContactDogovor"]:
            $dogovor_no = $contact_custom_fields[$i]->values[0]->value;
            break;

    }
}

//$sms_content = 'Добрый день, ' . $contact_name . ".\n";
$sms_content .= "Подписан договор № " . $dogovor_no . ".\n";
$sms_content .= "Сумма заказа - " . $sum_of_goods . " тг., ";
$sms_content .= "аванс - " . $sum_of_prepaid_goods . " тг.\n";
$sms_content .= "Примерная сдача изделия - " . $formatted_planned_date . ".\n";
$sms_content .= "Спасибо, «Белый куб»";

$contact_phone = preg_replace("/[^0-9]/", "", $contact_phone);
$contact_phone = '7' . substr($contact_phone, -10);

$smsApi = new TargetSMS();

$response = $smsApi->sendSMS($sms_content, $contact_phone);

print_r($response);

function sendMail($content)
{
    $from = "no-reply@koreanpony.kz";
    $headers = "From:" . $from . "\r\n";
//$headers .= 'Bcc: 41b0l123@gmail.com' . "\r\n";
    $headers .= "X-Mailer: PHP/" . phpversion();

    if (mail("aibolikmarketing@gmail.com", "WC Debug", json_encode($content), $headers)) {
        echo '<p style="color:#66A325;">Thanks! Your message has been sent.</p>';
    } else {
        echo '<p style="color:#F84B3C;">Something went wrong, go back and try again!</p>';
    }

}

/* Результат запроса сохраняется в свойстве "result" объекта \AmoCRM\Handler()
Содержит в себе объект, полученный от AmoCRM, какой конкретно - сверяйтесь с документацией для каждого метода
Ошибка запроса выбросит исключение */
//$api->result == false, если ответ пустой (то есть контакты с таким телефоном не найдены) */

?>
