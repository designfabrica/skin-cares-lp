<?php

use \AmoCRM\Handler;

require('libs/amoapi.php');
require('libs/trelloapi.php');
require('libs/targetsmsapi.php');

$api = new Handler('whitecube2', 'aibolikdev@gmail.com');

$lead_id = "";

if (isset($_POST)) {
    $lead_id = $_POST["leads"]["status"][0]["id"];
} else {
    exit("Invalid leadId");
}

$lead = getLead($lead_id, $api);

$lead_custom_fields = $lead->custom_fields;
$contact_id = $lead->main_contact_id;
$responsible_manager_id = $lead->responsible_user_id;

$contact = getContact($lead->main_contact_id, $api);

$contact_phone = "";
$contact_custom_fields = $contact->custom_fields;

for ($i = 0; $i < count($contact_custom_fields); $i++) {
    if ($contact_custom_fields[$i]->id == $api->config["Phone"]) {
        $contact_phone = $contact_custom_fields[$i]->values[0]->value;
        break;
    }
}

$manager = getResponsibleManager($responsible_manager_id, $api);

$sms_content = "Ваш заказ принят в разработку. ";
$sms_content .= "Ваш менеджер " . $manager->name . ", ";
$sms_content .= "телефон - " . $manager->phone_number . ", +77273545604.";
$sms_content .= "\nСпасибо, «Белый куб»";

//$lead_custom_fields = $result->contacts[0]->custom_fields;

$contact_phone = preg_replace("/[^0-9]/", "", $contact_phone);
$contact_phone = '7' . substr($contact_phone, -10);

$smsApi = new TargetSMS();

$response = $smsApi->sendSMS($sms_content, $contact_phone);

print_r($response);

//sendMail($sms_content . "\n\n" . $contact_phone);

function sendMail($content) {
    $from = "no-reply@koreanpony.kz";
    $headers = "From:" . $from . "\r\n";
//$headers .= 'Bcc: 41b0l123@gmail.com' . "\r\n";
    $headers .= "X-Mailer: PHP/" . phpversion();

    if (mail ("aibolikmarketing@gmail.com", "WC Debug", json_encode($content), $headers)) {
        echo '<p style="color:#66A325;">Thanks! Your message has been sent.</p>';
    } else {
        echo '<p style="color:#F84B3C;">Something went wrong, go back and try again!</p>';
    }

}

/* Результат запроса сохраняется в свойстве "result" объекта \AmoCRM\Handler()
Содержит в себе объект, полученный от AmoCRM, какой конкретно - сверяйтесь с документацией для каждого метода
Ошибка запроса выбросит исключение */
//$api->result == false, если ответ пустой (то есть контакты с таким телефоном не найдены) */

?>
