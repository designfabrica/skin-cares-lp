<?php

use \AmoCRM\Handler;
use \AmoCRM\Request;

require('libs/amoapi.php');
require('libs/trelloapi.php');

/* Создание экземпляра API, где "domain" - имя вашего домена в AmoCRM, а
"user@example.com" - email пользователя, от чьего имени будут совершаться запросы */
$api = new Handler('whitecube2', 'aibolikdev@gmail.com');

/* Создание экземляра запроса */

$lead_id = "";

if (isset($_POST)) {
    $lead_id = $_POST["leads"]["status"][0]["id"];
} else {
    exit("Invalid leadId");
}

$request_get = new Request(Request::GET, ['id' => $lead_id], ['leads', 'list']);

/* Выполнение запроса */
$result = $api->request($request_get)->result;

$custom_fields = $result->leads[0]->custom_fields;
$contactId = $result->leads[0]->main_contact_id;
$responsible_manager_id = $result->leads[0]->responsible_user_id;

$manager = getResponsibleManager($responsible_manager_id, $api);

$installPlannedDate = "";
$installPlanDays = "";
$sumOfGoods = "";
$sumOfPrepaidGoods = "";
$prilojeniyeNo = "";
$prihodnikNo = "";
$orderNo = "";
$izdeliya = "";

for ($i = 0; $i < count($custom_fields); $i++) {
    switch ($custom_fields[$i]->id) {
        case $api->config["InstallPlannedDate"]:
            $installPlannedDate = $custom_fields[$i]->values[0]->value;
            break;
        case $api->config["InstallPlanDays"]:
            $installPlanDays = $custom_fields[$i]->values[0]->value;
            break;
        case $api->config["SumOfGoods"]:
            $sumOfGoods = $custom_fields[$i]->values[0]->value;
            break;
        case $api->config["SumOfPrepaidGoods"]:
            $sumOfPrepaidGoods = $custom_fields[$i]->values[0]->value;
            break;
        case $api->config["PrilojeniyeNo"]:
            $prilojeniyeNo = $custom_fields[$i]->values[0]->value;
            break;
        case $api->config["PrihodnikNo"]:
            $prihodnikNo = $custom_fields[$i]->values[0]->value;
            break;
        case $api->config["OrderNo"]:
            $orderNo = $custom_fields[$i]->values[0]->value;
            break;
        case $api->config["Izdeliya"]:
            for ($j = 0; $j < count($custom_fields[$i]->values); $j++) {
                $izdeliya .= $custom_fields[$i]->values[$j]->value;
                if ($j + 1 != count($custom_fields[$i]->values)) {
                    $izdeliya .= ",";
                }
            }
            break;
    }
}

$date = date_create($installPlannedDate);
$formatted_planned_date = date_format($date, 'Y/m/d');

// Достакм контакта, чтобы взять его имя
$request_get = new Request(Request::GET, ['id' => $contactId], ['contacts', 'list']);
$result = $api->request($request_get)->result;
$contact_name = $result->contacts[0]->name;

$contact_dogovor = "";
$dogovor_date = "";
$contact_phone = "";
$object_address = "";

$custom_fields = $result->contacts[0]->custom_fields;

for ($i = 0; $i < count($custom_fields); $i++) {
    switch ($custom_fields[$i]->id) {
        case $api->config["ObjectAddress"]:
            $object_address = $custom_fields[$i]->values[0]->value;
            break;
        case $api->config["Phone"]:
            $contact_phone = $custom_fields[$i]->values[0]->value;
            break;
        case $api->config["ContactDogovor"]:
            $contact_dogovor = $custom_fields[$i]->values[0]->value;
            break;
        case $api->config["DogovorDate"]:
            $dogovor_date = $custom_fields[$i]->values[0]->value;
            break;
    }
}

$dogovor_date = date_create($dogovor_date);
$formatted_dogovor_date = date_format($dogovor_date, 'Y/m/d');

$card_description = "Клиент: " . $contact_name . "\n";
$card_description .= "Изделие: " . $izdeliya . "\n";
$card_description .= "Адрес: " . $object_address . "\n";
$card_description .= "Телефон: " . $contact_phone . "\n";
$card_description .= "Дата завершения: " . $formatted_planned_date . "\n";

$card_description .= "\n";

$card_description .= "Менеджер: " . $manager->name . "\n";
$card_description .= "Договор: " . "№" . $prilojeniyeNo . " к " . $contact_dogovor . " от " . $formatted_dogovor_date .  "\n";
$card_description .= "ПКО: " . $prihodnikNo . "\n";
$card_description .= "ЧЕК: " . $sumOfGoods . " тг.\n";
$card_description .= "Аванс: " . $sumOfPrepaidGoods . " тг.\n";

//$card_description .= "Дата планируемой установки: " . $formattedPlannedDate . "\n";
//$card_description .= "Срок планируемой установки: " . $installPlanDays . "\n";
//$card_description .= "Сумма приложении: " . $sumOfGoods . " тг.\n";
//$card_description .= "Срок предоплаты: " . $sumOfPrepaidGoods . " тг.\n";
//$card_description .= "Номер договора, №: " . $dogovorNo . "\n";
//$card_description .= "Номер приложения, №: " . $prilojeniyeNo . "\n";
//$card_description .= "Дата договора: " . $dogovorDate . "\n";
//$card_description .= "Номер приходника, №: " . $prihodnikNo . "\n";
//$card_description .= "Номер заказа, №: " . $orderNo . "\n";

$trello = new Trello();
$idList = "58dcf78f2c745d20e66e7ea7";

$list_name = $contact_dogovor . "_" . $prilojeniyeNo . " - " . $contact_name . " - " . $izdeliya;

$newcard = array("name" => $list_name, "desc" => $card_description, "idList" => $idList, "due" => $formatted_planned_date);

$response = $trello->createCard($newcard);

print_r($response);


function sendMail($content) {
    $from = "no-reply@koreanpony.kz";
    $headers = "From:" . $from . "\r\n";
//$headers .= 'Bcc: 41b0l123@gmail.com' . "\r\n";
    $headers .= "X-Mailer: PHP/" . phpversion();

    if (mail ("aibolikmarketing@gmail.com", "WC Debug", json_encode($content), $headers)) {
        echo '<p style="color:#66A325;">Thanks! Your message has been sent.</p>';
    } else {
        echo '<p style="color:#F84B3C;">Something went wrong, go back and try again!</p>';
    }

}

/* Результат запроса сохраняется в свойстве "result" объекта \AmoCRM\Handler()
Содержит в себе объект, полученный от AmoCRM, какой конкретно - сверяйтесь с документацией для каждого метода
Ошибка запроса выбросит исключение */
//$api->result == false, если ответ пустой (то есть контакты с таким телефоном не найдены) */

?>
